<?php
/**
 * @file
 * xi_vanilla_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function xi_vanilla_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
